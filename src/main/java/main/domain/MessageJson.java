package main.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageJson {
	private String status;
	private Map<String, List<String>> message;
	
	public MessageJson(String status, Map<String, List<String>> message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	
	public MessageJson() {
		this.status = "";
		this.message = new HashMap<>();
	}


	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Map<String, List<String>> getMessage() {
		return message;
	}
	public void setMessage(Map<String, List<String>> message) {
		this.message = message;
	}
	
	
}
