package main.domain;

import java.util.ArrayList;
import java.util.List;

public class MessageString {
	private String status;
	private List<String> message;
	
	public MessageString(String status, List<String> message) {
		super();
		this.status = status;
		this.message = message;
	}
	
	
	public MessageString() {
		this.status = "";
		this.message = new ArrayList<String>();
	}


	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getMessage() {
		return message;
	}
	public void setMessage(List<String> message) {
		this.message = message;
	}
}
