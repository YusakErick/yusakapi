package main.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown=true)
public class Dogs {
	private String breed;
	private List<Dogs> subbreed;
	
	public Dogs() {
		this.breed = "";
		this.subbreed = null;
	}
	
	public Dogs(String breed, List<Dogs> subbreed) {
		this.breed = breed;
		this.subbreed = subbreed;
	}
	
	public String getBreed() {
		return breed;
	}
	
	public void setBreed(String breed) {
		this.breed = breed;
	}
	
	public List<Dogs> getSubbreed() {
		return subbreed;
	}
	
	public void setSubbreed(List<Dogs> subbreed) {
		this.subbreed = subbreed;
	}
	
	
	
}
