package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Restconfiguration {
	
	public static void main(String[] args) {
        SpringApplication.run(Restconfiguration.class, args);
	}

}
