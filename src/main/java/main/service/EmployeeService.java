package main.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.tomcat.util.security.Escape;
import org.springframework.stereotype.Service;

import main.domain.Employee;
import main.mapper.EmployeeMapper;

@Service
public class EmployeeService {
	
	public void save(Employee e) {
		SqlSession sqlSession=MyBatisUtil.getSqlSessionFactory().openSession();
		try{
			EmployeeMapper employeeMapper=sqlSession.getMapper(EmployeeMapper.class);
			employeeMapper.save(e);
			employeeMapper.savesalary(e);
			sqlSession.commit();
		}catch(Exception ex){
			sqlSession.rollback();
			ex.printStackTrace();
			System.out.println("rollback");
		}finally{
			sqlSession.close();
		}
	}
	
	public Employee getemployee(int e) {
		SqlSession sqlSession=MyBatisUtil.getSqlSessionFactory().openSession();
		try{
			EmployeeMapper employeeMapper=sqlSession.getMapper(EmployeeMapper.class);
			return employeeMapper.getemploye(e);
		}catch(Exception ex){
			sqlSession.rollback();
			ex.printStackTrace();
			System.out.println("rollback");
		}finally{
			sqlSession.close();
		}
		return null;
	}
	

	public void deleteemployee(int e) {
		SqlSession sqlSession=MyBatisUtil.getSqlSessionFactory().openSession();
		try{
			EmployeeMapper employeeMapper=sqlSession.getMapper(EmployeeMapper.class);
			employeeMapper.delete(e);
			employeeMapper.deletesalary(e);
			sqlSession.commit();
		}catch(Exception ex){
			sqlSession.rollback();
			ex.printStackTrace();
		}finally{
			sqlSession.close();
		}
	}
	
	public void update(Employee e) {
		SqlSession sqlSession=MyBatisUtil.getSqlSessionFactory().openSession();
		try{
			EmployeeMapper employeeMapper=sqlSession.getMapper(EmployeeMapper.class);
			employeeMapper.update(e);
			sqlSession.commit();
		}catch(Exception ex){
			sqlSession.rollback();
			ex.printStackTrace();
			System.out.println("rollback");
		}finally{
			sqlSession.close();
		}
	}
	
	public List<Employee> getall() {
		SqlSession sqlSession=MyBatisUtil.getSqlSessionFactory().openSession();
		try{
			EmployeeMapper employeeMapper=sqlSession.getMapper(EmployeeMapper.class);
			return employeeMapper.getallemploye();
		}catch(Exception ex){
			sqlSession.rollback();
			ex.printStackTrace();
			System.out.println("rollback");
		}finally{
			sqlSession.close();
		}
		return null;
	}
}
