package main.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import main.domain.Dogs;
import main.domain.Dogsfamily;
import main.domain.MessageJson;
import main.domain.MessageString;


@Service
public class Dogsservice {
	
	public List<Dogs> getalldogs() {
		String url = "https://dog.ceo/api/breeds/list/all";
		RestTemplate resttemplate = new RestTemplate();
		MessageJson msg1 = resttemplate.getForObject(url, MessageJson.class);
		url = "https://dog.ceo/api/breeds/list";
		MessageString msg2 = resttemplate.getForObject(url, MessageString.class);
		List<Dogs> doggo = new ArrayList<Dogs>();
		for(String breed : msg2.getMessage()) {
			Dogs dog = new Dogs();
			dog.setBreed(breed);
			List<Dogs> subdog = new ArrayList<Dogs>();
			if(msg1.getMessage().get(breed).size()!=0) {
				for(String subbread : msg1.getMessage().get(breed)) {
					Dogs Sdog = new Dogs();
					Sdog.setBreed(subbread);
					subdog.add(Sdog);
				}
			}
			dog.setSubbreed(subdog);
			doggo.add(dog);
		}
		return doggo;
	}

	public Dogsfamily getdogsbreed(String breed) {
		Dogsfamily doge = new Dogsfamily();
		String url = "https://dog.ceo/api/breed/"+breed+"/list";
		RestTemplate resttemplate = new RestTemplate();
		MessageString msg = resttemplate.getForObject(url, MessageString.class);
		doge.setBreed(breed);
		List<Dogs> subdog = new ArrayList<Dogs>();
		for(String subbreed : msg.getMessage()) {
			Dogs Sdog = new Dogs();
			Sdog.setBreed(subbreed);
			subdog.add(Sdog);
		}
		doge.setSubbreed(subdog);
		List<String> img = new ArrayList<String>();
		url = "https://dog.ceo/api/breed/"+breed+"/images";
		msg = resttemplate.getForObject(url, MessageString.class);
		for(String image : msg.getMessage()) {
			img.add(image);
		}
		doge.setImage(img);
		return doge;
	}
}
