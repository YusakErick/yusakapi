package main.mapper;

import java.util.List;

import main.domain.Employee;

public interface EmployeeMapper {
	public void save(Employee e );
	public void savesalary(Employee e );
	public void update(Employee e );
	public List<Employee> getallemploye();
	public Employee getemploye(int id);
	public void delete(int id);
	public void deletesalary(int id);
}
