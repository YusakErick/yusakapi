package main.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import main.domain.Dogs;
import main.domain.Dogsfamily;
import main.service.Dogsservice;

@RestController
@Api(value="Dogs", description="this is Dogs API")
public class DogsController {
	
	@Autowired
	private Dogsservice dogservice;

	@RequestMapping(value="/dogs",method= RequestMethod.GET)
	public List<Dogs> getDogs(){
		return dogservice.getalldogs();
	}
	
	@RequestMapping(value="/dogs/{breed}",method= RequestMethod.GET)
	public Dogsfamily getDogsbreed(@PathVariable String breed){
		return dogservice.getdogsbreed(breed);
	}
}
