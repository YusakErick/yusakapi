package main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import main.domain.Employee;
import main.service.EmployeeService;

@RestController
@Api(value="employee", description="this is Employee API")
public class EmployeeController {
	@Autowired
	private EmployeeService es;
	
	public EmployeeController() {
		es = new EmployeeService();
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/employee")
	public List<Employee> getallemployee(){
		return es.getall();
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/employee/")
	public void newemployee(@RequestBody Employee e){
		es.save(e);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/employee/{id}")
	public void saveemployee(@PathVariable int id,@RequestBody Employee e) {
		e.setId(id);
		Employee oldE = es.getemployee(id);
		if(!e.getFull_name().equals(oldE.getFull_name()))
			e.setFull_name(oldE.getFull_name());
		
		if(!e.getAddress().equals(oldE.getAddress()))
			e.setAddress(oldE.getAddress());
		
		if(!e.getDob().equals(oldE.getDob()))
			e.setDob(oldE.getDob());
		
		if((e.getRole_id()==oldE.getRole_id())||(e.getRole_id()==0))
			e.setRole_id(oldE.getRole_id());
		
		if((e.getSalary()==oldE.getSalary())||(e.getSalary()==0))
			e.setSalary(oldE.getSalary());
		
		es.update(e);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/employee/{id}")
	public Employee getemployee(@PathVariable int id) {
		return es.getemployee(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/employee/{id}")
	public void deleteemployee(@PathVariable int id) {
		es.deleteemployee(id);;
	}
}
